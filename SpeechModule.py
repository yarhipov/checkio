FIRST_TEN = ["zero", "one", "two", "three", "four", "five", "six", "seven",
             "eight", "nine"]
SECOND_TEN = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
              "sixteen", "seventeen", "eighteen", "nineteen"]
OTHER_TENS = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy",
              "eighty", "ninety"]
HUNDRED = "hundred"


def checkio(number):
    count_handred = number // 100
    result = []
    if count_handred>0:
        result = [FIRST_TEN[count_handred], HUNDRED]
        number = number - (count_handred * 100)

    count_other = number // 10
    if count_other > 1:
        result.append(OTHER_TENS[count_other-2])
        number = number - (count_other * 10)

    elif count_other == 1:
        count_second = number % 10
        result.append(SECOND_TEN[count_second])
        number = 0

    if number > 0:
        result.append(FIRST_TEN[number])

    return ' '.join(result)

