def checkio(els):
    return sum(els[:3])

if checkio([1, 2, 3, 4, 5, 6]) == 6:
    print('Done!')