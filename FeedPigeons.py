def checkio(number):
    pigeons = [1, ]
    delta = 2
    for p in pigeons:
        pigeons.append(p + delta)
        delta += 1
        number = number - p
        if number - pigeons[-1] < 0:
            if number > p:
                return number
            return p



#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio(1) == 1, "1st example"
    assert checkio(2) == 1, "2nd example"
    assert checkio(5) == 3, "3rd example"
    assert checkio(10) == 6, "4th example"
    assert checkio(3) == 2, "5th example"
