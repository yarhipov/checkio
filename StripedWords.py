import re

VOWELS = "AEIOUY"
CONSONANTS = "BCDFGHJKLMNPQRSTVWXZ"



def checkio(text):
    pattern = re.compile(r'\w+')
    double_vowels = re.compile(r'['+VOWELS+']['+VOWELS+']')
    double_consonants = re.compile(r'['+CONSONANTS+']['+CONSONANTS+']')
    one_vowels = re.compile(r'^['+VOWELS+']$')
    one_consonants = re.compile(r'^['+CONSONANTS+']$')
    digit = re.compile(r'\d')


    count = 0

    for word in pattern.findall(text.upper()):

        if not double_vowels.search(word) and \
                not double_consonants.search(word) and \
                not one_vowels.search(word) and \
                not one_consonants.search(word) and \
                not digit.search(word):
            count += 1

    return count

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio("My name is ...") == 3, "All words are striped"
    assert checkio("Hello world") == 0, "No one"
    assert checkio("A quantity of striped words.") == 1, "Only of"
    assert checkio("Dog,cat,mouse,bird.Human.") == 3, "Dog, cat and human"