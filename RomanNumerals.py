
class Foo():
    data = 0
    result = []
    number = {
        1: 'I',
        4: 'IV',
        5: 'V',
        9: 'IX',
        10: 'X',
        40: 'XL',
        49: 'IL',
        50: 'L',
        90: 'XC',
        100: 'C',
        400: 'CD',
        500: 'D',
        900: 'CM',
        1000: 'M'}

    def check_number(self, number, count):
        c = self.data // number
        if (c > 0) and (c <= count):
            self.result.append(self.return_number(self.number[number], c))
            self.data = self.data - (c * number)

    def return_number(self, number, count):
        result = []
        for i in range(count):
            result.append(number)
        return ''.join(result)


    def checkio(self, data):
        self.data =data
        self.result = []

        self.check_number(1000, 3)
        self.check_number(900, 1)
        self.check_number(500, 1)
        self.check_number(400, 1)
        self.check_number(100, 3)
        self.check_number(90, 1)
        self.check_number(50, 1)
        self.check_number(49, 1)
        self.check_number(40, 1)
        self.check_number(10, 3)
        self.check_number(9, 1)
        self.check_number(5, 1)
        self.check_number(4, 1)
        self.check_number(1, 3)

        return ''.join(self.result)

def checkio(data):
    return Foo().checkio(data)

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio(3) == 'III', '3'
    assert checkio(4) == 'IV', '4'
    assert checkio(5) == 'V', '5'
    assert checkio(6) == 'VI', '6'
    assert checkio(76) == 'LXXVI', '76'
    assert checkio(499) == 'CDXCIX', '499'
    assert checkio(3888) == 'MMMDCCCLXXXVIII', '3888'