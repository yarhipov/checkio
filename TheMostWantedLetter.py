import re

def checkio(text):
    text = text.lower()
    pattern = re.compile('\w')
    result = dict()
    for w in {n for n in pattern.findall(text)}:
        count = len(re.findall(w, text))
        v = result.get(count, [])
        v.append(w)
        result[count] = v
    result = result[sorted(result)[-1]]
    result.sort()

    return result[0]


#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio("Hello World!") == "l", "Hello test"
    assert checkio("How do you do?") == "o", "O is most wanted"
    assert checkio("One") == "e", "All letter only once."
