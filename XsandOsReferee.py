




def checkio(game_result):
    rows = [game_result[0],
            game_result[1],
            game_result[2],
            ''.join([game_result[0][0], game_result[1][1], game_result[2][2]]),
            ''.join([game_result[0][2], game_result[1][1], game_result[2][0]]),
            ''.join([game_result[0][0], game_result[1][0], game_result[2][0]]),
            ''.join([game_result[0][1], game_result[1][1], game_result[2][1]]),
            ''.join([game_result[0][2], game_result[1][2], game_result[2][2]]),
            ]
    for s in rows:
        if s == "XXX":
            return "X"
        elif s == "OOO":
            return "O"
    return "D"


    return "D" or "X" or "O"

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio([
        "X.O",
        "XX.",
        "XOO"]) == "X", "Xs wins"
    assert checkio([
        "OO.",
        "XOX",
        "XOX"]) == "O", "Os wins"
    assert checkio([
        "OOX",
        "XXO",
        "OXX"]) == "D", "Draw"
